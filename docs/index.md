---
hide:
  - navigation
  - toc
---
# Algoritma dan Pemrograman

[Buka Bukunya Di sini](pengantar.md){ .md-button .md-button--primary }

Halo, selamat datang di buku online untuk matakuliah Algoritma dan Pemrograman.  Buku ini sebagai referensi dalam matakuliah Algoritma dan Pemrograman di Program Studi Informatika, Fakultas Teknologi Informasi, Universitas Kristen Duta Wacana, Yogyakarta.

Lisensi buku online ini adalah [Atribusi-NonKomersial 4.0 Internasional (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/deed.id).

![](images/logokampus.png) 

### Profil Penulis

![](images/penulis.jpg)

Modul mata kuliah Struktur Data ini disusun oleh Yuan Lukito, S.Kom., M.Cs.  Saat ini terdaftar sebagai dosen di Program Studi Informatika, Fakultas Teknologi Informasi, Universitas Kristen Duta Wacana.  Bidang minat yang ditekuni adalah Machine Learning, Algorithmic Trading dan Software Development.  

Mata kuliah yang pernah diampu antara lain:

* Algoritma dan Pemrograman
* Struktur Data
* Pemrograman Berorientasi Obyek
* Supervised Learning
* Internet of Things
* Pemrograman Web
* Pemrograman iOS

Profil akademis sebagai dosen dapat dilihat pada halaman web berikut ini:

* [Scopus ID: 57202220282](https://www.scopus.com/authid/detail.uri?authorId=57202220282)
* [Google Scholar](https://scholar.google.co.id/citations?user=fQuDHsMAAAAJ)
* [ResearchGate](https://www.researchgate.net/profile/Yuan-Lukito-2)
* [Sinta Ristekbrin](https://sinta.ristekbrin.go.id/authors/detail/?id=257549&view=overview)

Email: yuanlukito[at]ti.ukdw.ac.id, Telegram: @yuanlukito

