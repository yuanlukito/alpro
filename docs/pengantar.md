# Kata Pengantar

Buku ini merupakan referensi utama dari matakuliah Algoritma dan Pemrograman yang diselenggarakan di Program Studi Informatika, Universitas Kristen Duta Wacana.  

Isi dari buku ini sudah disesuaikan dengan kurikulum 2021 yang saat ini berlaku di Program Studi Informatika.  Bahasa pemrograman yang dipakai sebagai acuan dalam buku ini adalah Python. Mengapa Python? Bahasa pemrograman Python merupakan salah satu bahasa pemrograman yang "ramah" terhadap pemula dan memiliki *learning curve* yang relatif rendah.  Sintaks Python lebih sederhana jika dibandingkan dengan bahasa pemrograman lain seperti Java, C, C++ atau C#.  Popularitas Python juga semakin meningkat, faktor utamanya karena banyak dipakai pada bidang *Data Science* dan *Machine Learning*. 

Materi dan contoh-contoh dan soal latihan yang ada di dalam buku ini telah diperbaharui dan disesuaikan dengan kasus-kasus yang banyak ditemui, khususnya pada bidang ilmu yang berkaitan dengan algoritma dan pemrograman.  Walaupun penyusunan buku ini telah dilakukan dengan sebaik-baiknya dan dilakukan dalam beberapa tahap revisi, dimungkinkan masih ada kekurangan-kekurangan yang ada.  Kami sangat berterima kasih jika anda dapat menyampaikan kekurangan-kekurangan tersebut sehingga bisa diperbaiki pada revisi berikutnya.

Akhir kata, selamat belajar algoritma dan pemrograman.  Semoga anda bisa mencapai tujuan belajar dari buku ini.

**Yogyakarta, Januari 2022**

**Penulis**